package com.baomidou.springboot.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.springboot.helper.DescCatoryHelper;
import com.baomidou.springboot.service.IHtmlFetchService;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * 读取阿里详情页
 * Created by zcy on 2017/7/6.
 */
@Service
public class HtmlFetchServiceImpl implements IHtmlFetchService{
    @Override public String fetchAliHtmlDetail(String url) {
        String html=null;
        try {
            Document doc = getAliDefaultConnet(url);
            boolean praseFlag=DescCatoryHelper.requestDesc(doc);
            if(!praseFlag)throw new Exception();
            html=doc.html();
        } catch (Exception e) {
            logger.error(String.format("解析错误url：{}",url),e);
        }
        return html;
    }

    @Override public void changeCookies(String cookie) {
        String[] arr=cookieValue.split(";");
        for(String cookiestr:arr){
            String[] cookiearr=cookiestr.split("=");
            if(cookiearr.length==2){
                cookies.put(cookiearr[0], cookiearr[1]);
            }else{
                cookies.put(cookiearr[0], "");
            }

        }
    }

    protected static Logger logger = Logger.getLogger(HtmlFetchServiceImpl.class);
    public static long startTime=0L;
    private static Map<String,String> cookies=new HashMap<String, String>();
        public static String cookieValue="cna=tga8EbBcvXMCAT2kLOIAUaEW; JSESSIONID=9L78akHi1-UTuXiEthutRMvitEX6-n3QzANQ-5; ali_apache_tracktmp=c_w_signed=Y; UM_distinctid=15ce2219995513-00d8cba4ea4315-30637509-fa000-15ce2219998335; ali_ab=61.164.44.227.1498442470640.7; ali_apache_track=c_ms=1|c_mt=3|c_mid=b2b-2135715960|c_lid=kongjishisecom; ali_beacon_id=125.119.97.245.1499350606582.345152.8; _is_show_loginId_change_block_=b2b-2135715960_false; _show_force_unbind_div_=b2b-2135715960_false; _show_sys_unbind_div_=b2b-2135715960_false; _show_user_unbind_div_=b2b-2135715960_false; cookie1=VWfEichDBdglNOu8pp9a%2FLSihLDtcEXIBhqKkHW%2Bc%2BI%3D; cookie2=1c54510eb7f68ae9d16bfdfdc56f378c; cookie17=UUkNYalu8hFUIg%3D%3D; uss=AnRZQcp1plin0Qpi47zWdC7%2BXTddoDcfF0%2B9ufY3geMr5GEqEZQl99b1; t=0679cabbfa957448f2686b7c77adc54d; _tb_token_=e7d1381011475; sg=m08; m_id=b2b-2135715960; m_level=PM.PENDING; m_sign=8590066005; __cn_logon__=true; __cn_logon_id__=kongjishisecom; cn_tmp=Z28mC+GqtZ0s9gBWlWQ3oKZL3kJv01YZSxJPP0Npl7CGE5apwtkL/+GRO3vYOWrhXTlTpK2rvHq9O1LOIA7AF9+rlIAKrJui2i/OdKRKsK27oqilIWKlbwQ3Hcb1Ks1cVugNbzxGe3BjijYdS3YnlP9/dA56IdvpGydqsUDyIZnj4I+nJBg6lYDrP6sCjiihnZf80Fpf7gVOL7XaX17cXspoUfy0H+FXVqohQTtgBRyAe79W+RmyVR5BaWsHdCoa; _cn_slid_=m0Ql%2BnA5vS; tbsnid=HpUGYhtMs%2BLRqKu8BXdiv9KnL2EQJK0UwGZGZZcvBz46sOlEpJKl9g%3D%3D; LoginUmid=okboxHiH%2BNnymw6bMzdHDuvYtHgURhnlfwmlW8OKvwJ0ZvS19%2BaAOg%3D%3D; userID=TXCwIFJjO7nDcjS6V3o2J0nzxCTzjlzdJ4zVn7jahM86sOlEpJKl9g%3D%3D; last_mid=b2b-2135715960; unb=2135715960; __last_loginid__=kongjishisecom; login=kFeyVBJLQQI%3D; _csrf_token=1499439745399%2FfH2oYIhWD2hm7A; __utmt=1; __utma=62251820.2072789624.1498039596.1499435748.1499439747.9; __utmb=62251820.1.10.1499439747; __utmc=62251820; __utmz=62251820.1499219763.5.2.utmcsr=fuwu.1688.com|utmccn=(referral)|utmcmd=referral|utmcct=/; userIDNum=Q66meEyBOf1nhRthAUduHA%3D%3D; _nk_=4XqXIR1qHwaJZNpI%2BUE0Kw%3D%3D; __rn_alert__=false; _tmp_ck_0=t9wOA%2BIRt2DAuRIZTe1xiFEbi1JaMh3Coemj0rDSHcJOn92r9HS3wexo%2BleBsZMajJpjXqE2Szj3r77i7jq3nhOB81iCjrrGy8OS%2B8XN5zQ2LZntsjkxiqhqGJm%2B3BCCwRwKA5Et6E1zZyQt4VDi%2FL8ntNPzwdApXyHREIVDlZMijuVCLWpeuNRSuWhotCkkNhnUk%2FPQl6IFAjkFvjRl%2B7nZ8Ik1gZMHwETBn3QjNJzduyQhlnebbkXM65bUwqNcF5H0LWoT6ugeT1HDi9l9AFkkjdlqIluliQR%2FLEJ3HWCT4XWrKt0jwzirlcUVFCbLRgpJo680k0n20LyUByjBeVo5NYjUPMJxFE9CrVx1f0ZnhO%2FRGWef3Ln21eggEqJoEpPdmzC31eboE%2F0x%2F6oQrS9SEjjL7sC2wjlwpVHUJJPgU0yixwdhoVwzR04JS8RWx2wS8WQ6qdn4GDnnQdhW25jqJ0Y0PG700af1yAeqv4XkpIOiYyVeRW99JvBzGKTOJO19tuh4Tzza1MvwIadvaKm4nDlVlZ2Jdt%2BdZ27O6y1JwJ4mBiY27g%3D%3D; alicnweb=touch_tb_at%3D1499438772202%7Clastlogonid%3Dkongjishisecom%7Cshow_inter_tips%3Dfalse; isg=Aq2teGpdiFN5bWyqGn-dDzQ3vE_nouDb8MCDKO-y6cSzZs0Yt1rxrPs0Jgx7";
    static{
        String[] arr=cookieValue.split(";");
        for(String cookiestr:arr){
            int index=cookiestr.indexOf("=");
            cookies.put(cookiestr.substring(0,index),cookiestr.substring(index,cookiestr.length()) );

        }
    }




    private static synchronized Document getAliDefaultConnet(String url) throws IOException, InterruptedException{
        long endTime=System.currentTimeMillis();
        if(endTime-startTime>=450){

        }else{
            Thread.sleep(450-endTime+startTime);
        }
        startTime=endTime;
        Document doc = parseUrl(url);
        logger.info(url);
        return doc;
    }

    private static Document parseUrl(String url){
        Document doc =null;
        try {
            doc = Jsoup.connect(url).cookies(cookies).header("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:54.0) Gecko/20100101 Firefox/54.0").timeout(20000).get();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return doc;
    }


    public static void main(String[] args) throws IOException, InterruptedException {
        HtmlFetchServiceImpl htmlFetchService = new HtmlFetchServiceImpl();
        String doc=htmlFetchService.fetchAliHtmlDetail("https://detail.1688.com/offer/548705596794.html");
        System.out.println(doc);
    }
}



package com.baomidou.springboot.service;

/**
 * 详情页面获取
 * Created by zcy on 2017/7/6.
 */
public interface IHtmlFetchService {

    String fetchAliHtmlDetail(String url);

    void changeCookies(String cookie);
}

package com.baomidou.springboot.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * 项目的配置
 * Created by zcy on 2017/7/10.
 */
@Configuration
@ConfigurationProperties(prefix = SpyerConfig.PREFIX)
public class SpyerConfig {
    public static final String PREFIX="spyer";

    /**
     * cookies的值  
     */
    private String cookies;

    private String loginName;

    private String password;

    private String chromeDriverPath;

    public String getCookies() {
        return cookies;
    }

    public void setCookies(String cookies) {
        this.cookies = cookies;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getChromeDriverPath() {
        return chromeDriverPath;
    }

    public void setChromeDriverPath(String chromeDriverPath) {
        this.chromeDriverPath = chromeDriverPath;
    }
}

package com.baomidou.springboot.controller;

import com.baomidou.springboot.service.IHtmlFetchService;
import com.baomidou.springboot.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 页面获取器
 * Created by zcy on 2017/7/6.
 */
 @RestController
 @RequestMapping("/htmlfetch")
public class HtmlFetchController {

     @Autowired
     private IHtmlFetchService htmlFetchService;

    /**
     * 1688详情页html获取
     * @param url
     * @return
     */
     @RequestMapping("/ali")
     public String ali(@RequestParam("url") String url){
         return htmlFetchService.fetchAliHtmlDetail(url);
     }


     @RequestMapping("/changeCookie")
    public void changeCookie(String cookie){
         //htmlFetchService
     }
}

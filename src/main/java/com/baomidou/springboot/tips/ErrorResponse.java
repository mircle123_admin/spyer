package com.baomidou.springboot.tips;

import com.baomidou.springboot.exception.BizExceptionEnum;

/**
 * Created by zcy on 2017/8/6.
 */
public class ErrorResponse extends Response{


    public ErrorResponse(BizExceptionEnum biz){
        this.tip=new ErrorTip(biz);
    }

}

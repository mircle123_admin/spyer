package com.baomidou.springboot.tips;

/**
 * Created by zcy on 2017/8/6.
 */
public abstract class Response {
    protected Tip tip;
    protected Object data;

    public Tip getTip() {
        return tip;
    }

    public void setTip(Tip tip) {
        this.tip = tip;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}

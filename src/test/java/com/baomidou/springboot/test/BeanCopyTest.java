package com.baomidou.springboot.test;

import lombok.Data;
import org.junit.Test;
import org.springframework.beans.BeanUtils;

/**
 * Created by zcy on 2017/8/11.
 */
public class BeanCopyTest {

    @Test
    public void testCopy(){
        Student student = new Student();
        student.setName("张三");
        student.setAge(18);
        student.setCount(1);
        Teacher teacher = new Teacher();
        BeanUtils.copyProperties(student,teacher);
        System.out.println(teacher);
    }

    @Data
    class Student{
        private String name;
        private Integer age;
        private Integer count;


    }
    @Data
    class Teacher{
        private String name;
        private Integer age;
        private String desc;


    }
}
